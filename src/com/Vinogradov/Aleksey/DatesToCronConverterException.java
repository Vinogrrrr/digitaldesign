package com.Vinogradov.Aleksey;

public class DatesToCronConverterException extends Exception{
    String message;
    public DatesToCronConverterException(String message) {
        super(message);
    }
}
