package com.Vinogradov.Aleksey;

import java.util.List;

public interface DatesToCronConverter {
    String convert(List<String> dates) throws DatesToCronConverterException;

    String getImplementationInfo();
}
